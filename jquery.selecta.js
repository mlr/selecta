/*
* Selecta
* Makes select elements prettier with custom div-based dropdown
* Ronnie Miller (me@ronniemlr.com)
* http://ronniemlr.com/jquery/selecta/
*/
(function($) {
$.fn.selecta = function() {
  return this.each(function() {
    if(!$(this).is('select')) return;

    var select          = $(this);
    var selected_val    = select.val();
    var selected_option = select.find('option:selected');
    var options         = select.children('option');
    var multiple        = !!select.attr('multiple');
    var options_items   = "";

    for(i=0; i<options.length; i++) {
      option = options.eq(i);
      options_items += '<div class="selecta_option'+(option.val() == selected_val ? ' selecta_current_option' : '')+'" data-value="'+option.val()+'">'+option.html()+'</div>';
    }

    // Create selecta and attach it next to the original select
    $('<div class="selecta '+select.attr('id')+'">')
      .append('<div class="selecta_selected_option" data-value="'+selected_option.val()+'">'+(selected_option.html() || "Please select")+'</div>')
      .append('<div class="selecta_options" style="display: none;">'+options_items+'</div>')
      .find('.selecta_selected_option').click(function() {
        show_selecta = $(this).parent().find('div.selecta_options');
        $('div.selecta_options').not(show_selecta).hide(); // hide other selecta instances
        show_selecta.slideToggle(100);
      }).end()
      .find('.selecta_option').click(function() {
        // If multi-select, then unselect the option if it's selected
        if(multiple && $(this).hasClass('selecta_current_option')) {
          $(this).removeClass('selecta_current_option');
          select.find("option:eq("+$(this).index()+")").removeAttr('selected');
        } else {
          $(this).addClass('selecta_current_option');
          select.find("option:eq("+$(this).index()+")").attr('selected', 'selected');

          // If we can't select multiple options, then unselect the rest
          if(!multiple) {
            $(this).siblings().removeClass('selecta_current_option');
            select.find("option:not(:eq("+$(this).index()+"))").removeAttr('selected');
            select.val($(this).attr('data-value'));
          }
        }

        // update selected option text and hide
        selected_options = select.find("option:selected");
        total_selected   = selected_options.length;
        selected_text    = (total_selected > 1) ? total_selected + " selected" : selected_options.html() || "Please select";
        $(this).parentsUntil('.selecta').parent().find('.selecta_selected_option').html(selected_text);
        $(this).parent().slideUp('fast');

        // Notify the select element it has changed
        select.trigger('change');
      }).end()
      .insertAfter(select);

    // Hide the original select box
    select.addClass('has_selecta').hide();

    // Hide all selectas when another field is the focus
    $('input, select, textarea').focus(function() {
      $('.selecta_options').slideUp('fast');
    });
  });
}
})(jQuery);
