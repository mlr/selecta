# Selecta

Selecta is a jQuery plugin that progressively enhances HTML &lt;select&gt; tags.

[See the demo](http://ronniemlr.com/jquery/selecta/ "jQuery selecta demo")
